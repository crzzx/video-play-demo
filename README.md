# video-play-demo

## 介绍
视频上传和下载的demo基本代码模板。
- 前端为vue2及其原生播放器video
- 后端使用视频流和文件机制实现视频传输
- gitee地址：https://gitee.com/crzzx/video-play-demo.git

## 前端部署

![image-20230724150319114](https://s2.loli.net/2023/07/24/sncIbLqFiajxGgw.png)

### 1.基本安装与使用
#### （1）安装
```bash
npm install
```

#### （2）运行

```bash
npm run dev
```



### 2.页面及代码介绍

#### （1）首页（上传页）

![image-20230724150127059](https://s2.loli.net/2023/07/24/4vjYwFMuHV2xesP.png)

> 代码使用逻辑见代码注释。



#### （2）单片、分片等下载页

![image-20230724150817635](https://s2.loli.net/2023/07/24/wUiX1LdNxlcMH8v.png)

> 三者对比，单片最快，虽说是单片，但它会根据用户点击的进度条动态向后端获取视频流，没错，这很**”边缓存边播放“**。而且速度最快，只要一点击播放按钮，就会立刻可播放，这个是我在后续测试中发现的。
>
> 分片的话，下载最彻底，他是所有分片都下好之后才允许播放，但缺点就是不够灵活。
>
> 另一个就算了，不看也可，因为根据我最新发现，单片足以做到这个想要达到的效果。



## 后端部署

### 1.配置和使用

#### （1）Pom依赖（应用于上传下载）

```bash
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>javax.servlet-api</artifactId>
    <version>4.0.1</version> <!-- 使用适合您的Tomcat版本的API版本 -->
    <scope>provided</scope>
</dependency>
```

#### （2）数据库

![image-20230724151326056](https://s2.loli.net/2023/07/24/lCzSD4ARIqEWrgs.png)

![image-20230724151337171](https://s2.loli.net/2023/07/24/EzgiD9nfFCcbMWB.png)

#### （3）其他及注意点

> 基本按照前后端代码对应关系看就可以明确如何使用了。

**注意点**

> 注意这个启动类中文件的限制大小要与前端一致，如图：
>
> ![image-20230724151706454](https://s2.loli.net/2023/07/24/xtnqTlP5IDOJspc.png)
>
> ![image-20230724151748553](https://s2.loli.net/2023/07/24/Lhjl7Ju35CMKSWr.png)