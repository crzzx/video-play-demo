package com.example.videodemo.config;

/**
 * @version 1.0
 * @Author ZZX
 * @Date 2023/7/8 18:01
 */

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer
{

    @Override
    public void addCorsMappings(CorsRegistry registry)
    {
        registry.addMapping("/**").allowedOriginPatterns("*") // 允许所有来源（使用通配符*）
                .allowCredentials(true) // 允许凭据（如Cookies、认证头等）
                .allowedMethods("GET", "POST", "PUT", "DELETE") // 允许的请求方法
                .allowedHeaders("*"); // 允许的请求头，可以自行添加需要的请求头
    }
}

