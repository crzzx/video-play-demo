package com.example.videodemo;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.util.unit.DataSize;
import org.springframework.context.annotation.Bean;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication
//这里写的是告诉spring框架mapper接口在什么位置，然后找到对应的地方扫描。不写框架会找不到接口
@MapperScan("com.example.videodemo.mapper")
public class VideoDemoApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(VideoDemoApplication.class, args);
    }


    /**
     * 文件上传配置
     *
     * @return
     */
    @Bean
    public MultipartConfigElement multipartConfigElement()
    {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //单个文件最大
        factory.setMaxFileSize(DataSize.parse("5000MB")); //KB,MB
        /// 设置总上传数据总大小
        factory.setMaxRequestSize(DataSize.parse("5000MB"));
        return factory.createMultipartConfig();
    }

}

